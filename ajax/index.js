fetch('https://ajax.test-danit.com/api/swapi/films')
  .then(response => response.json())
  .then(films => {
    films.forEach(film => {
      const filmTitle = document.createElement('h2');
      filmTitle.textContent = `Episode ${film.episodeId}: ${film.name}`;
      document.body.appendChild(filmTitle);

      const filmSummary = document.createElement('p');
      filmSummary.textContent = `Summary: ${film.openingCrawl}`;
      document.body.appendChild(filmSummary);

      const charactersList = document.createElement('ul');
      document.body.appendChild(charactersList);

      film.characters.forEach(characterUrl => {
        fetch(characterUrl)
          .then(response => response.json())
          .then(character => {
            const characterItem = document.createElement('li');
            characterItem.textContent = character.name;
            charactersList.appendChild(characterItem);
          })
          .catch(error => console.error('Error:', error));
      });
    });
  })
  .catch(error => console.error('Error:', error));
